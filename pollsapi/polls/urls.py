from django.urls import path

from .apiviews import PollList, PollDetail,\
    ChoiceList, CreateVote, UserCreate, LoginView

urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("", PollList.as_view(), name="polls_list"),
    path("<int:pk>/", PollDetail.as_view(), name="polls_detail"),
    path("<int:pk>/choices/", ChoiceList.as_view(), name="choice_list"),
    path("<int:pk>/choices/<int:choice_pk>/vote/", CreateVote.as_view(), name="create_vote"),
    path("users/", UserCreate.as_view(), name="user_create"),
]
